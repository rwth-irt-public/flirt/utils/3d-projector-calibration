""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

from pyquaternion import Quaternion
import cv2 as cv2
import numpy as np


class CalibrationResult:
    """
    Stores the calibration information of a monocular camera.
    Helps saving the inforamtion to an yaml file.
    """

    def __init__(self):
        self.image_size = (640, 480)
        self.camera_name = "T470p_webcam"
        self.reprojection_error = 0.
        self.camera_matrix = np.array([[647.91745594, 0.0, 325.95594983],
                                       [0.0, 644.1504564, 275.38039303],
                                       [0.0, 0.0, 1.0]], dtype=np.float32)
        self.distortion_model = "rational_polynomial"
        self.distortion_coefficients = np.array(
            [[-0.41527, 0.31874, -0.00197, 0.00071, 0]])
        self.projection_matrix = np.array(
            [[647.91745594, 0.0, 325.95594983, 0.0],
             [0.0, 644.1504564, 275.38039303, 0.0],
             [0.0, 0.0, 1.0, 0.0]], dtype=np.float32)
        self.rotation_matrix = np.array([[1, 0, 0],
                                         [0, 1, 0],
                                         [0, 0, 1]], dtype=np.float32)
        self.translation_vector = np.array([0, 0, 0])
        # alpha parameter of getOptimalNewCameraMatrix
        self.alpha = 0

    def __repr__(self):
        return ("name: {}\nreprojection_error: {}\nwidth: {}\nheight: {}\n"
                "K:\n{}\nd:\n{}\nP:\n{}\nR:\n{}\nT:\n{}").format(
            self.reprojection_error, self.camera_name, self.image_size[0],
            self.image_size[1], self.camera_matrix,
            self.distortion_coefficients, self.projection_matrix,
            self.rotation_matrix, self.translation_vector)

    def __str__(self):
        return ("name: {}\nreprojection_error: {}\nwidth: {}\nheight: {}\n"
                "K:\n{}\nd:\n{}\nP:\n{}\nR:\n{}\nT:\n{}").format(
            self.reprojection_error, self.camera_name, self.image_size[0],
            self.image_size[1], self.camera_matrix,
            self.distortion_coefficients, self.projection_matrix,
            self.rotation_matrix, self.translation_vector)

    def create_projection_matrix(self):
        """
        Calculates the ROS projection matrix which is the result of
        cv2.getOptimalNewCameraMatrix.
        See this see this https://wiki.ros.org/image_pipeline/CameraInfo

        Parameters
        ----------
        alpha : double
            Scaling parameter between 0 (all pixels in undistorted image valid)
            and 1 (all source pixels retained in undistorted image)
        t : InputArray
            position relative to base camera
        """
        t = np.reshape(self.translation_vector, (3, 1))
        # https://github.com/dimatura/ros_vimdoc/blob/master/doc/ros-camera-info.txt
        new_K, _ = cv2.getOptimalNewCameraMatrix(
            self.camera_matrix,
            self.distortion_coefficients,
            self.image_size,
            self.alpha)
        transform = np.zeros((3, 4), dtype=np.float32)
        transform[:3, :3] = np.identity(3)
        transform[:3, 3:4] = t
        self.projection_matrix = new_K.dot(transform)

    def load(filename):
        """
        Loads the calibration from a file

        Returns
        -------
        The loaded CalibrationResult
        """
        file = cv2.FileStorage(filename, cv2.FileStorage_READ,
                               encoding="utf-8")
        calib = CalibrationResult()
        image_width = int(file.getNode("image_width").real())
        image_height = int(file.getNode("image_height").real())
        calib.image_size = (image_width, image_height)
        calib.camera_name = file.getNode("camera_name").string()
        calib.reprojection_error = file.getNode("reprojection_error").real()
        calib.camera_matrix = file.getNode("camera_matrix").mat()
        calib.distortion_model = file.getNode("distortion_model").string()
        calib.distortion_coefficients = file.getNode(
            "distortion_coefficients").mat()
        calib.alpha = file.getNode("alpha").real()
        calib.projection_matrix = file.getNode("projection_matrix").mat()
        calib.rotation_matrix = file.getNode("rotation_matrix").mat()
        calib.translation_vector = file.getNode("translation_vector").mat()
        file.release()
        return calib

    def save(self, filename):
        file = cv2.FileStorage(filename, cv2.FileStorage_WRITE,
                               encoding="utf-8")
        file.write("image_width", self.image_size[0])
        file.write("image_height", self.image_size[1])
        file.write("camera_name", self.camera_name)
        file.write("reprojection_error", self.reprojection_error)
        file.write("camera_matrix", self.camera_matrix)
        file.write("distortion_model", self.distortion_model)
        file.write("distortion_coefficients", self.distortion_coefficients)
        file.write("alpha", self.alpha)
        file.write("projection_matrix", self.projection_matrix)
        file.write("rotation_matrix", self.rotation_matrix)
        file.write("translation_vector", self.translation_vector)
        # additional representations for the transformation
        # quaternion rotation, pyquaternion: w,x,y,z ROS: x,y,z,w
        rot = Quaternion(matrix=self.rotation_matrix)
        file.write("quaternion", np.array([rot[1], rot[2], rot[3], rot[0]]))
        tf_matrix = np.identity(4)
        tf_matrix[:3, :3] = self.rotation_matrix
        tf_matrix[:3, 3:4] = self.translation_vector
        file.write("tf_matrix", tf_matrix)
        file.release()


def main():
    calib_storage = CalibrationResult()
    calib_storage.create_projection_matrix()
    calib_storage = CalibrationResult.load(
        "data/projector_1920x1080_calib.yml")
    calib_storage.save("data/test.yml")
    print(calib_storage)


# run the main prgram
if __name__ == "__main__":
    main()
