""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2
from calibration_result import CalibrationResult
import numpy as np


def collect_data(detector, calib_params):
    """
    Collect the data for the calibrator

    Parameters
    ----------
    detector : Detector
        Provides a next_frame method that detect the calibration in the next
        frame and calculates the object points
    calib_params : CalibrationParamters
        parameters of the calibration

    Returns
    -------
    cam_image_points : OutputArrayOfArrays
        the accumulated image points for the camera
    projector_image_points : OutputArrayOfArrays
        the accumulated image points for the projector
    object_points : OutputArrayOfArrays
        the accumulated object points in the camera frame
    """
    # create the display
    cv2.namedWindow("visualization", cv2.WINDOW_NORMAL)
    # collect the data
    accum_cam_pts = []
    accum_proj_pts = []
    accum_obj_pts = []
    # number of detected views
    n_views = 0
    # number of frames received from the camera
    n_frames = -1
    while (n_views < calib_params.n_views
           and cv2.waitKey(calib_params.wait_time) & 0xFF != ord('q')):
        n_frames += 1
        # skip the number of skip_frames
        if n_frames % (calib_params.skip_frames + 1) != 0:
            detector.skip_frame()
            continue
        # process next frame
        frame, cam_img_pts, proj_img_pts, obj_pts = detector.next_frame()
        if (cam_img_pts is not None
                and proj_img_pts is not None
                and obj_pts is not None):
            n_views += 1
            accum_cam_pts.append(cam_img_pts)
            accum_proj_pts.append(proj_img_pts)
            accum_obj_pts.append(obj_pts)
        # draw the results
        cv2.putText(frame, "view number {} of {}".format(
            n_views, calib_params.n_views), (30, 30),
            cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0))
        cv2.imshow("visualization", frame)
    return accum_cam_pts, accum_proj_pts, accum_obj_pts


def calibrate_projector(cam_img_pts, proj_img_pts, obj_pts, calib_params):
    """
    Calculates the intrinsic and extrinsic projector parameters

    Parameters
    ----------
    cam_img_pts : InputArrayOfArrays
        the image points of the camera
    proj_img_pts : InputArrayOfArrays
        the image points of the projector
    obj_pts : InputArrayOfArrays
        the object points in the camera frame
    calib_params : CalibrationParameters
        contains the cameras intrinsics and projector image_size

    Returns
    -------
    stereo_error : number
        the reprojection error of the stereo calibration
    K_proj : OutputArrayOfArrays
        the projectors intrinsics matrix
    d_proj : OutputArrayOfArrays
        the projectors distortion coefficients
    R_proj : OutputArrayOfArrays
        rotation matrix between the 1st and the 2nd camera coordinate systems
    t_proj : OutputArrays
        translation vector between the 1st and the 2nd camera coordinate
        systems
    """
    if (cam_img_pts and proj_img_pts and obj_pts):
        # projector intrinsics guess
        image_width = calib_params.image_size[0]
        image_height = calib_params.image_size[1]
        K_proj = np.array([[image_width, 0, image_width / 2],
                           [0, image_width, image_height / 2],
                           [0, 0, 1]], dtype=np.float32)
        d_proj = np.array([0, 0, 0, 0, 0], dtype=np.float32)
        # calibrate the intrinsics of the projector first
        proj_error, K_proj, d_proj, _, _ = cv2.calibrateCamera(
            obj_pts, proj_img_pts, calib_params.image_size, K_proj, d_proj,
            flags=cv2.CALIB_USE_INTRINSIC_GUESS + cv2.CALIB_RATIONAL_MODEL)
        # stereo calibration for projector extrinsics using the intrinsics
        # determined before and CALIB_FIX_INTRINSICS flag
        (stereo_error, K_cam, d_cam, K_proj, d_proj,
            R_proj, t_proj, _, _) = cv2.stereoCalibrate(
            obj_pts, cam_img_pts, proj_img_pts, calib_params.K_cam,
            calib_params.d_cam, K_proj, d_proj, calib_params.image_size,
            flags=cv2.CALIB_FIX_INTRINSIC + cv2.CALIB_RATIONAL_MODEL)
        return stereo_error, K_proj, d_proj, R_proj, t_proj
    else:
        return None


def save_calibration(calib_params, calib_result):
    """
    Save the results of the calibration to a file

    Parameters
    ----------
    calib_params : CalibrationParams
        Prameters for the calibration (name, image_size, ...)
    calibr_result : Tuple
        (error, K, d, R, T)
    """
    if not calib_result:
        print("calibration result is None")
        return
    error, K, d, R, T = calib_result
    calib_result = CalibrationResult()
    calib_result.image_size = calib_params.image_size
    calib_result.camera_name = calib_params.name
    calib_result.reprojection_error = error
    calib_result.camera_matrix = K
    calib_result.distortion_coefficients = d
    calib_result.rotation_matrix = R
    calib_result.translation_vector = T
    calib_result.create_projection_matrix()
    print("reprojection error: {}".format(error))
    calib_result.save("data/{}_calib.yml".format(
        calib_params.name))
    print(calib_result)


def execute_calibration(detector, calib_params):
    """
    Camera calibration using a video file or device.

    Parameters
    ----------
    detector : Detector
        Provides a next_frame method that detect the calibration in the next
        frame and calculates the object points
    calib_params : CalibrationParams
        Prameters for the calibration (name, image_size, ...)
    """
    try:
        cam_pts, proj_pts, obj_pts = collect_data(detector, calib_params)
        calib_result = calibrate_projector(
            cam_pts, proj_pts, obj_pts, calib_params)
        save_calibration(calib_params, calib_result)
    finally:
        cv2.destroyAllWindows()
