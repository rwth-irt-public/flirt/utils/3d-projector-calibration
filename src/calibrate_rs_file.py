""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

from calibration_params import CalibrationParams
from depth_camera_detector import DepthCameraDetector
from pattern_generation import create_full_projection_grid
from projector_calibration import execute_calibration
from rs_camera import RsCamera

"""
Calibration using the recorded realsense bag file
"""
calib_params = CalibrationParams.load("data/projector_1920x1080_params.yml")
camera = RsCamera.from_bag_file("data/projector_1920x1080.bag")
circles_grid = create_full_projection_grid(calib_params.image_size)
detector = DepthCameraDetector(camera, circles_grid)
execute_calibration(detector, calib_params)
