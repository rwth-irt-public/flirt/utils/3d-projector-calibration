""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """


class DepthCameraDetector:
    """
    Detects image and object points using a depth camera.
    """

    def __init__(self, depth_camera, calibration_pattern):
        """
        Parameters
        ----------
        depth_camera : DepthCamera
            provides read_aligned_depth_to_color and deproject_points methods
        calibration_pattern : CalibrationPattern
            displayed on projector, provides detect_image_points,
            draw_image_points and get_object_points methods
        """
        self.camera = depth_camera
        self.pattern = calibration_pattern

    def next_frame(self):
        """
        Detects the object points of the circles pattern using the realsense
        aligned depth image.

        Returns
        -------
        frame : Image
            the annotated color image of the detection
        cam_image_points : OutputArrayOfArrays
            the image points of the color camera or None on fail
        proj_image_points : OutputArrayOfArrays
            the image_points of the projector or None on fail
        object_points : OutputArrayOfArrays
            the object points in the camera frame or None on fail
        """
        # get the next frames from the realsense
        retval, color_frame, aligned_depth_frame = (
            self.camera.read_aligned_depth_to_color())
        if not retval:
            return color_frame, None, None, None
        # detect the projectors calibration pattern
        found_pattern, image_points = self.pattern.detect_image_points(
            color_frame)
        if not found_pattern:
            return color_frame, None, None, None
        # calculate the object points
        points_are_valid, object_points = self.camera.deproject_points(
            image_points, aligned_depth_frame)
        if points_are_valid:
            # visualize
            color_frame = self.pattern.draw_image_points(color_frame)
            return (color_frame, image_points, self.pattern.get_image_points(),
                    object_points)
        else:
            return color_frame, None, None, None

    def skip_frame(self):
        """
        Reads the next frame but does not process it.


        Returns
        -------
        frame : Image
            the annotated color image of the detection
        """
        _, color_frame, _ = (self.camera.read_aligned_depth_to_color())
        return color_frame
