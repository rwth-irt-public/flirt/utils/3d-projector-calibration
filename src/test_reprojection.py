""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2
from calibration_result import CalibrationResult
import numpy as np
import pattern_generation as pgen
from depth_camera_detector import DepthCameraDetector
from rs_camera import RsCamera

"""
Test the calibration by reprojecting the circles grid into the projectors
image.
"""

# init detection and calibration paramters
calib_result = CalibrationResult.load("data/projector_1920x1080_calib.yml")
camera = RsCamera.from_device()
pattern = pgen.create_full_projection_grid(calib_result.image_size)
detector = DepthCameraDetector(camera, pattern)
K_cam, d_cam = camera.get_intrinsics()
K_proj = calib_result.camera_matrix
d_proj = calib_result.distortion_coefficients
R_proj = calib_result.rotation_matrix
t_proj = calib_result.translation_vector
# draw the pattern
cv2.namedWindow("projector", cv2.WINDOW_NORMAL)
cv2.namedWindow("reprojection", cv2.WINDOW_NORMAL)
cv2.namedWindow("camera", cv2.WINDOW_NORMAL)
print("move the pattern window to the desired screen")
cv2.waitKey()
cv2.setWindowProperty("projector", cv2.WND_PROP_FULLSCREEN,
                      cv2.WINDOW_FULLSCREEN)
cv2.imshow("projector", pattern.draw(calib_result.image_size))
# test on every incoming image
while cv2.waitKey(200) & 0xFF != ord('q'):
    frame, _, _, obj_pts = detector.next_frame()
    proj_image = pattern.draw(calib_result.image_size)
    # 3D for color image
    proj_image = np.dstack((proj_image, proj_image, proj_image))
    if obj_pts is not None:
        # project onto the camera image
        # rvec = tvec = np.array([[0, 0, 0]], np.float32)
        # cam_image_points = cv2.projectPoints(
        #     obj_pts, rvec, tvec, K_cam, d_cam)
        # for image_point in cam_image_points[0]:
        #     frame = cv2.circle(frame, (image_point[0, 0], image_point[0, 1]),
        #                        3, 255, cv2.FILLED)
        # project onto the circles image
        proj_image_points = cv2.projectPoints(
            obj_pts, R_proj, t_proj, K_proj, d_proj)
        for image_point in pattern.get_image_points():
            proj_image = cv2.circle(
                proj_image, (image_point[0], image_point[1]), 4,
                (0, 255, 0), cv2.FILLED)
        for image_point in proj_image_points[0]:
            proj_image = cv2.circle(
                proj_image, (image_point[0, 0], image_point[0, 1]), 4,
                (0, 0, 255), cv2.FILLED)
    cv2.imshow("camera", frame)
    cv2.imshow("reprojection", proj_image)
