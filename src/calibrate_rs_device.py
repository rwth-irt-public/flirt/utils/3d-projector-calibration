""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2
from calibration_params import CalibrationParams
from depth_camera_detector import DepthCameraDetector
from pattern_generation import create_full_projection_grid
from projector_calibration import execute_calibration
from rs_camera import RsCamera

"""
Calibration using the connect realsense device and projector
"""
camera = RsCamera.from_device()
calib_params = CalibrationParams.load("data/projector_1920x1080_params.yml")
circles_grid = create_full_projection_grid(calib_params.image_size, False)
# draw pattern on projector
cv2.namedWindow("projector", cv2.WINDOW_NORMAL)
cv2.namedWindow("visualization", cv2.WINDOW_NORMAL)
print("move the pattern window to the desired screen")
cv2.waitKey()
cv2.setWindowProperty("projector", cv2.WND_PROP_FULLSCREEN,
                      cv2.WINDOW_FULLSCREEN)
cv2.imshow("projector", circles_grid.draw(calib_params.image_size))
# calibrate using the depth camera detector
detector = DepthCameraDetector(camera, circles_grid)
execute_calibration(detector, calib_params)
