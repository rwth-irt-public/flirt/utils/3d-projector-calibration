""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2 as cv2
import numpy as np


def draw_circles(image_size, circle_centers, radius=30, color=255,
                 background=0):
    """
    Draws an image containing the circles.

    Parameters
    ----------
    image_size : Size
        (width, height)
    cicle_centers : Point
        positions of the circles centers
    radius : int
        radius of the circles
    color : int
        color of the circles
    background : int
        color of the background

    Returns
    -------
    image : OutputArrayOfArrays
        image containing the circls gird
    """
    image_width = int(image_size[0])
    image_height = int(image_size[1])
    # postion of the points in the image
    image = np.full((image_height, image_width), background,
                    dtype=np.uint8)
    for point in circle_centers:
        image = cv2.circle(image, tuple(point), int(radius), color, cv2.FILLED)
    return image


class CirclesGrid:
    def __init__(self, is_symmetric, pattern_size, spacing,
                 border=0, color=255, background=0):
        """
        Create the circles grid

        Parameters
        ----------
        is_symmetric : bool
            symmetric or asymmetric circles grid
        pattern_size : Size
            number of circles per row and column (points_per_row,
            points_per_colum) or equally (columns, rows)
        spacing: number
            spacing of the grid (half the distance between circle centers)
        border : number
            additional border on the left and top
        color : int
            color of the circles
        background : int
            color of the background
        """
        self.is_symmetric = is_symmetric
        self.pattern_size = pattern_size
        self.spacing = spacing
        self.border = border
        self.color = color
        self.background = background

    def save(self, filename):
        file = cv2.FileStorage(filename, cv2.FileStorage_WRITE,
                               encoding="utf-8")
        file.write("is_symmetric", self.is_symmetric)
        file.write("pattern_size", self.pattern_size)
        file.write("spacing", self.spacing)
        file.write("border", self.border)
        file.write("color", self.color)
        file.write("background", self.background)
        file.release()

    def load(filename):
        file = cv2.FileStorage(filename, cv2.FileStorage_READ,
                               encoding="utf-8")
        is_symmetric = bool(file.getNode("is_symmetric").real())
        pattern_size = file.getNode("pattern_size").mat()
        pattern_size = (int(pattern_size[0]), int(pattern_size[1]))
        spacing = file.getNode("spacing").real()
        border = file.getNode("border").real()
        color = int(file.getNode("color").real())
        background = int(file.getNode("background").real())
        file.release()
        return CirclesGrid(is_symmetric, pattern_size, spacing, border, color,
                           background)

    def detect_image_points(self, image):
        """
        Detects the circle centers in the image.

        Parameters
        ----------
        image : InputArrayOfArrays
            possibly containing this circles grid

        Returns
        -------
        found_grid : bool
            True if the grid has been detected
        circle_centers : OutputArrayOfArrays
            The position of the circle centers in the image
        """
        # blob detector for the right color
        blob_params = cv2.SimpleBlobDetector_Params()
        blob_params.blobColor = self.color
        blob_detector = cv2.SimpleBlobDetector_create(blob_params)
        if self.is_symmetric:
            return cv2.findCirclesGrid(
                image, self.pattern_size,
                flags=cv2.CALIB_CB_SYMMETRIC_GRID | cv2.CALIB_CB_CLUSTERING,
                blobDetector=blob_detector)
        else:
            return cv2.findCirclesGrid(
                image, self.pattern_size,
                flags=cv2.CALIB_CB_ASYMMETRIC_GRID | cv2.CALIB_CB_CLUSTERING,
                blobDetector=blob_detector)

    def draw(self, image_size, radius=30):
        """
        Draws an image containing the circles.

        Parameters
        ----------
        image_size : Size
            (width, height)
        radius : int
            radius of the circles

        Returns
        -------
        image : OutputArrayOfArrays
            image containing the circls gird
        """
        return draw_circles(image_size, self.get_image_points(), radius,
                            self.color, self.background)

    def draw_image_points(self, image):
        """
        Draws the circle centers to the image if any have been detected

        Parameters
        ----------
        image : InputArrayOfArrays
            possibly containing this circles grid

        Returns
        -------
        image : OutputArrayOfArrays
            possibly annotated image
        """
        found_grid, centers = self.detect_image_points(image)
        return cv2.drawChessboardCorners(image, self.pattern_size, centers,
                                         found_grid)

    def get_image_points(self):
        """
        The locations of the circle centers in 2D.

        Returns
        -------
        grid : OutputArrayOfArrays
            the center points of the grid (X, Y)
        """
        columns = self.pattern_size[0]
        rows = self.pattern_size[1]
        points = np.zeros((0, 2), dtype=np.float32)
        for row in range(0, rows):
            for column in range(0, columns):
                if self.is_symmetric:
                    # create symmetric pattern
                    point = np.array(
                        [[self.border + column * self.spacing,
                            self.border + row * self.spacing]],
                        dtype=np.float32)
                else:
                    # create asymmetric pattern
                    if row % 2 == 1:
                        # offset for every even column
                        offset = self.spacing
                    else:
                        offset = 0
                    # distance is spacing times two
                    point = np.array(
                        [[self.border + offset + column * self.spacing * 2,
                          self.border + row * self.spacing]],
                        dtype=np.float32)
                # add point to the list
                points = np.append(points, point, axis=0)
        return points

    def get_object_points(self):
        """
        Creates the locations of the circle centers as planar 3D object points.

        Returns
        -------
        grid : OutputArrayOfArrays
            the center points of the grid (X, Y, Z=0)
        """
        centers_2d = self.get_image_points()
        centers_3d = np.zeros((0, 3), dtype=np.float32)
        for point in centers_2d:
            point_3d = np.array([[point[0], point[1], 0]], dtype=np.float32)
            centers_3d = np.append(centers_3d, point_3d, axis=0)
        return centers_3d


def main():
    grid = CirclesGrid(True, (11, 4), 90, border=66, color=150, background=42)
    grid.save("data/grid_test.yml")
    grid = CirclesGrid.load("data/grid_test.yml")
    cv2.imshow("circles grid", grid.draw((1080, 720)))
    cv2.waitKey()


# run the main prgram
if __name__ == "__main__":
    main()
