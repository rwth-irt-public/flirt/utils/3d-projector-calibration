""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import circles_grid as cg
import cv2.aruco as ar
import cv2 as cv2
import numpy as np


def create_default_charuco_board():
    """
    Creates the default charuco chessboard with 5x7 fields.
    The squares have 34mm and the markers 24mm length.

    Returns
    -------
    board : CharucoBoard
        the default board
    """
    # create the default board
    marker_dict = ar.getPredefinedDictionary(ar.DICT_6X6_50)
    return ar.CharucoBoard_create(5, 7, 0.034, 0.024, marker_dict)


def create_default_circles_grid():
    """
    Creates the default asymmetric 4 x 11 circles grid with 20mm spacing.

    Returns
    -------
    grid : CirclesGrid
        the generated asymmetric circles grid
    """
    return cg.CirclesGrid(False, (4, 11), 20, border=0, color=0,
                          background=255)


def create_left_projection_grid(image_size=(1920, 1080)):
    """
    Creates the default left half asymmetric grid with 90px spacing.
    The resolution is 1920 x 1080 pixels.

    Returns
    -------
    cricles_grid : CirclesGrid
        the generated asymmetric circles grid
    """
    _, cricles_grid = draw_left_grid(image_size, radius=30, spacing=90)
    return cricles_grid


def create_full_projection_grid(image_size=(1920, 1080), is_symmetric=False):
    """
    Creates the full size default asymmetric grid with 90px spacing.
    The resolution is 1920 x 1080 pixels.

    Returns
    -------
    cricles_grid : CirclesGrid
        the generated asymmetric circles grid
    """
    return create_largest_circles_grid(image_size, 90, is_symmetric)


def create_largest_circles_grid(image_size, spacing, is_symmetric):
    """
    Creates the largest circles grid that fits into the image_size.

    Parameters
    ----------
    image_size : Size
        (width, height)
    spacing : int
        distance between the circles

    Returns
    -------
    circles_grid : CirclesGrid
        the resulting CirclesGrid
    """
    image_width = image_size[0]
    image_height = image_size[1]
    # calculate the number of points that fit in the image
    points_per_column = int((image_height - spacing) / spacing)
    # detection requires uneven number of points per column
    if points_per_column % 2 == 0:
        points_per_column -= 1
    if is_symmetric:
        points_per_row = int((image_width - spacing) / (spacing))
    else:
        points_per_row = int((image_width - spacing) / (spacing * 2))
    # generate the grid
    return cg.CirclesGrid(is_symmetric, (points_per_row, points_per_column),
                          spacing=spacing, border=spacing)


def draw_left_grid(image_size, radius=30, spacing=90):
    """
    Draws the largest asymmetric circles grid that fits into the left half
    of image_size.

    Parameters
    ----------
    image_size : Size
        (width, height)
    radius : int
        radius of the circles
    spacing : int
        distance between the circles

    Returns
    -------
    image : OutputArrayOfArrays
        image containing the circls gird
    circles_grid : CirclesGrid
        the generated asymmetric circles grid
    """
    half_width = int(image_size[0] / 2)
    height = int(image_size[1])
    # draw circles half
    grid = create_largest_circles_grid((half_width, height),
                                       spacing=spacing, is_symmetric=False)
    circles_half = grid.draw((half_width, height))
    # draw empty half
    empty_half = np.zeros((height, half_width), dtype=np.uint8)
    # concatenate
    image = np.concatenate((circles_half, empty_half), axis=1)
    return image, grid


def main():
    board = create_default_charuco_board()
    image = board.draw((5 * 100, 7 * 100))
    cv2.imwrite("data/default_board.png", image)
    cv2.namedWindow("pattern generation", cv2.WINDOW_NORMAL)
    cv2.setWindowProperty("pattern generation", cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    image, circles_grid = draw_left_grid((1920, 1080))
    cv2.imshow("pattern generation", image)
    cv2.waitKey()


# run the main prgram
if __name__ == "__main__":
    main()
