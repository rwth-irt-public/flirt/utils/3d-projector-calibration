""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

from rs_camera import RsCamera
import numpy as np
import cv2
from calibration_params import CalibrationParams
import pattern_generation as pg


def show_frames(camera):
    ret, color, aligned_depth = camera.read_aligned_depth_to_color()
    if ret:
        # display image
        cv_depth_image = cv2.convertScaleAbs(aligned_depth, alpha=0.03)
        depth_colormap = cv2.applyColorMap(cv_depth_image, cv2.COLORMAP_JET)
        stacked_image = np.hstack((color, depth_colormap))
        cv2.imshow("rs frames", stacked_image)
    else:
        print("no frames arrived")


def record_bag():
    # display the circles pattern
    image_size = (1920, 1080)
    name = "projector_{}x{}".format(image_size[0], image_size[1])
    circles_grid = pg.create_full_projection_grid(image_size)
    cv2.namedWindow("pattern", cv2.WINDOW_NORMAL)
    cv2.namedWindow("rs frames", cv2.WINDOW_NORMAL)
    print("move the pattern window to the desired screen")
    cv2.waitKey()
    cv2.setWindowProperty("pattern", cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    cv2.imshow("pattern", circles_grid.draw(image_size))
    # Create pipeline for recording
    camera, recorder = RsCamera.for_record("data/{}.bag".format(name))
    recorder.resume()
    # stream and record
    while True:
        show_frames(camera)
        key = cv2.waitKey(1)
        # if pressed escape exit program
        if key == 27:
            break
    # save the circles grid
    circles_grid.save("data/{}_grid.yml".format(name))
    # save the parameters
    calib_params = CalibrationParams()
    calib_params.name = name
    calib_params.image_size = image_size
    K_cam, d_cam = camera.get_intrinsics()
    calib_params.K_cam = K_cam
    calib_params.d_cam = d_cam
    calib_params.save("data/{}_params.yml".format(name))


def playback_bag():
    # new pipeline for playback
    camera = RsCamera.from_bag_file("data/projector_1920x1080.bag")
    # stream and record
    while True:
        show_frames(camera)
        key = cv2.waitKey(1)
        # if pressed escape exit program
        if key == 27:
            break


try:
    # record_bag()
    playback_bag()
finally:
    cv2.destroyAllWindows()
