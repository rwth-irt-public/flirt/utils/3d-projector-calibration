""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

from calibration_result import CalibrationResult
from charuco_detector import get_charuco_corners, get_charuco_pose
import cv2 as cv2
import cv2.aruco as ar
import numpy as np
import pattern_generation as pg
from rs_camera import RsCamera

"""
Test the calibration by projecting the axis of a CharucoBoard.
"""

# create the video capture device
camera = RsCamera.from_device()
K_cam, d_cam = camera.get_intrinsics()
# init projector
projector_calib = CalibrationResult.load("data/projector_1920x1080_calib.yml")
K_proj = projector_calib.camera_matrix
d_proj = projector_calib.distortion_coefficients
d_zero = np.array([0, 0, 0, 0, 0], np.float)
R_proj = projector_calib.rotation_matrix
t_proj = projector_calib.translation_vector
projected_image = np.zeros((1080, 1920, 3), dtype=np.uint8)
# create board
board = pg.create_default_charuco_board()
# create the display
cv2.namedWindow("projector", cv2.WINDOW_NORMAL)
cv2.namedWindow("realsense", cv2.WINDOW_NORMAL)
print("move the pattern window to the desired screen")
cv2.waitKey()
cv2.setWindowProperty("projector", cv2.WND_PROP_FULLSCREEN,
                      cv2.WINDOW_FULLSCREEN)
cv2.imshow("projector", projected_image)
while cv2.waitKey(100) & 0xFF != 27:
    projected_image = np.zeros((1080, 1920, 3), dtype=np.uint8)
    ret, color_frame, depth_frame = camera.read_aligned_depth_to_color()
    if not ret:
        break
    # detect the board pose in camera image
    found_board, rvec, tvec = get_charuco_pose(color_frame, board, K_cam,
                                               d_cam)
    if found_board:
        # transform to projector
        R_mat, _ = cv2.Rodrigues(rvec)
        R_board = np.matmul(R_proj, R_mat)
        t_board = np.matmul(R_proj, tvec) + t_proj
        r_board, _ = cv2.Rodrigues(R_board)
        # draw axis
        color_frame = ar.drawAxis(color_frame, K_cam, d_cam, rvec,
                                  tvec, 0.034)
        projected_image = ar.drawAxis(projected_image, K_proj, d_proj, r_board,
                                      t_board, 0.034)
    # detect corners in camera image
    found_corners, charuco_corners, charuco_ids = get_charuco_corners(
        color_frame, board)
    if found_corners:
        # draw in realsense image
        color_frame = ar.drawDetectedCornersCharuco(
            color_frame, charuco_corners, charuco_ids)
        # deproject to 3D
        points_valid, object_points = camera.deproject_points(
            charuco_corners, depth_frame)
        if points_valid:
            project_points, _ = cv2.projectPoints(
                object_points, R_proj, t_proj, K_proj, d_proj)
            for point in project_points:
                point = np.reshape(point, (2, 1))
                projected_image = cv2.circle(
                    projected_image, tuple(point), 3, (0, 0, 255), cv2.FILLED)
    cv2.imshow("projector", projected_image)
    cv2.imshow("realsense", color_frame)
