""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2
import numpy as np
import pyrealsense2 as rs


def create_base_config():
    """
    Basic configuration for enabling color and depth streams
    """
    config = rs.config()
    config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)
    config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 6)
    return config


class RsCamera:
    """
    Reading color and aligned depth to color images from the realsense camera.
    Applies some basic filtering for improved depth quality.
    """

    def __init__(self, config, is_playback):
        """
        Create a realsense camera for a given configuration.
        Color and depth stream are expected to be enabled.
        Use the provided factory methods instead of this constructor!
        """
        # open video pipeline
        self.pipeline = rs.pipeline()
        self.profile = self.pipeline.start(config)
        # color image intrinsics
        color_stream = self.profile.get_stream(rs.stream.color)
        self.color_intrinsics = (color_stream.as_video_stream_profile()
                                 .get_intrinsics())
        # depth scaling
        self.depth_sensor = self.profile.get_device().first_depth_sensor()
        self.depth_scale = self.depth_sensor.get_depth_scale()
        self.max_depth = 2**16 - 1
        # Create an align depth to color object
        self.align = rs.align(rs.stream.color)
        # filters for depth images
        self.filters = [rs.disparity_transform(True), rs.spatial_filter(),
                        rs.disparity_transform(False),
                        rs.hole_filling_filter()]

    def __del__(self):
        self.pipeline.stop()

    def config_depth(self):
        """
        Configures the realsense camera for better depth quality.
        """
        self.depth_sensor.set_option(rs.option.visual_preset, 1)
        self.depth_sensor.set_option(rs.option.laser_power, 360)
        self.depth_sensor.set_option(rs.option.depth_units, 1e-4)
        self.depth_scale = self.depth_sensor.get_depth_scale()

    def for_record(filename):
        """
        Creates a realsense camera and records to the bag file

        Parameters
        ----------
        filename : string
            path to the .bag file

        Returns
        -------
        camera : RealsenseCamera
            the created realsense camera
        recorder : Recorder
            offers pause() and resume() methods to control what is recorded.
            is paused at start.
        """
        config = create_base_config()
        # configurate to read from bag file
        config.enable_record_to_file(filename)
        camera = RsCamera(config, False)
        # disable realtime to process all captured frames
        recorder = camera.profile.get_device().as_recorder()
        recorder.pause()
        # configure depth quality
        camera.config_depth()
        return camera, recorder

    def from_device():
        """
        Creates a realsense camera for the connected device.

        Returns
        -------
        camera : RealsenseCamera
            the created realsense camera
        """
        camera = RsCamera(create_base_config(), False)
        camera.config_depth()
        return camera

    def from_bag_file(filename):
        """
        Creates a realsense camera for bag the file.
        Realtime is disabled so all frames are polled.

        Parameters
        ----------
        filename : string
            path to the .bag file

        Returns
        -------
        camera : RealsenseCamera
            the created realsense camera
        """
        config = create_base_config()
        # configurate to read from bag file
        config.enable_device_from_file(filename)
        camera = RsCamera(config, True)
        # disable realtime to process all captured frames as fast as possible
        profile = camera.pipeline.get_active_profile()
        playback = profile.get_device().as_playback()
        playback.set_real_time(False)
        return camera

    def deproject_points(self, image_points, aligned_depth_image):
        """
        Converts the image points to 3D object points using the intrinsics.

        Parameters
        ----------
        image_points : InputArrayOfArrays
            2D Points (x, y)
        aligned_depth_image
            Shares intrinsics with the color camera, unscaled Z16 format

        Returns
        -------
        points_are_valid : bool
            True if all depth values are valid, False otherwise
        object_points : OutputArrayOfArrays
            the 3d object points
        """
        object_points = np.zeros((0, 3), dtype=np.float32)
        for image_point in image_points:
            image_point = np.reshape(image_point, (2,))
            depth = aligned_depth_image[int(image_point[1]),
                                        int(image_point[0])]
            # valid depth?
            if depth == 0 or depth == self.max_depth:
                return False, None
            depth_meters = depth * self.depth_scale
            # aligned depth to color image has color camera intrinsics
            object_point = rs.rs2_deproject_pixel_to_point(
                self.color_intrinsics, image_point.tolist(), depth_meters)
            object_point = np.array(object_point, dtype=np.float32)
            object_points = np.append(object_points, [object_point], axis=0)
        return True, object_points

    def get_intrinsics(self):
        """
        Returns the intrinsic parameters

        Returns
        -------
        K_cam : OutputArrayOfArrays
            the camera matrix
        d_cam : OutputArray
            the distortion coefficients
        """
        intr = self.color_intrinsics
        K_cam = np.array([[intr.fx, 0, intr.ppx],
                          [0, intr.fy, intr.ppy],
                          [0, 0, 1]], dtype=np.float32)
        d_cam = np.array(intr.coeffs, dtype=np.float32)
        return K_cam, d_cam

    def read(self):
        """
        Implements a read method similar to the OpenCV VideoCapture.read() for
        the color stream

        Returns
        -------
        retval : bool
            true if a valid frame has been received
        frame : OutputArrayOfArrays
            the color image
        """
        # decode the image
        got_frames, frames = self.pipeline.try_wait_for_frames()
        if not got_frames:
            return False, None
        color_frame = frames.get_color_frame()
        if not color_frame:
            return False, None
        # convert to numpy/opencv
        frame = np.asanyarray(color_frame.get_data())
        return True, frame

    def read_aligned_depth_to_color(self):
        """
        Implements a read method similar to the OpenCV VideoCapture.read() for
        the aligned depth to color stream.
        Reads the color and the depth frames synchronised.
        The depth frame is in raw Z16 format use, depth_scale!

        Returns
        -------
        retval : bool
            true if a valid frame has been received
        color_frame : OutputArrayOfArrays
            the color frame
        aligned_depth_frame : OutputArrayOfArrays
            the aligned depth to color frame
        """
        # decode the frames
        got_frames, frames = self.pipeline.try_wait_for_frames()
        if not got_frames:
            return False, None, None
        aligned_frames = self.align.process(frames)
        color_frame = aligned_frames.get_color_frame()
        aligned_depth_frame = aligned_frames.get_depth_frame()
        # did all frames arrive?
        if not color_frame or not aligned_depth_frame:
            return False, None, None
        # post processing
        for filter in self.filters:
            aligned_depth_frame = filter.process(aligned_depth_frame)
        # convert to numpy/opencv
        color_frame = np.asanyarray(color_frame.get_data())
        aligned_depth_frame = np.asanyarray(aligned_depth_frame.get_data())
        return True, color_frame, aligned_depth_frame


def main():
    """
    Quick test of the RealsenseCamera
    """
    realsense = RsCamera.from_device()
    print(realsense.color_intrinsics)
    print("depth scale {}".format(realsense.depth_scale))
    while cv2.waitKey(30) & 0xFF != ord('q'):
        ret, color, aligned_depth = realsense.read_aligned_depth_to_color()
        if ret:
            # display image
            cv_depth_image = cv2.convertScaleAbs(
                aligned_depth, alpha=0.03)
            depth_colormap = cv2.applyColorMap(
                cv_depth_image, cv2.COLORMAP_JET)
            stacked_image = np.hstack((color, depth_colormap))
            cv2.imshow("depth", stacked_image)
            # get some depth values
            image_points = [[200, 200], [960, 200]]
            points_are_valid, object_points = realsense.deproject_points(
                image_points, aligned_depth)
            if points_are_valid:
                print(object_points[:, 2])
            else:
                print("invalid pixels")
        else:
            print("no frames arrived")


if __name__ == "__main__":
    main()
