""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

import cv2
import cv2.aruco as ar
import numpy as np


def calc_object_points(image_points, rvec, tvec, K, d):
    """
    Calculates the 3D object points for the 2D image points.
    The plane of the points has to be known.
    Pretty graphical explaination of the maths:
    https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection

    Parameters
    ----------
    image_points : InputArray
        the points in image coordinates
    rvec : InputArray
        rotation vector of the board (Rodrigues angles)
    tvec : InputtArray
        translation vector of the board (is on the board)
    K : InputArray
        camera matrix
    d : InputArray
        distortion coefficients

    Returns
    -------
    object_points : OutputArray
        The 3D object points on the plane.
        None if the plane is almost parallel to the rays.
    """
    # result array
    object_points = np.zeros((0, 3), dtype=np.float32)
    # points in normalized image coordinates [-1,1]
    undistored_points = cv2.undistortPoints(image_points, K, d)
    normalized_points = cv2.convertPointsToHomogeneous(undistored_points)
    # define plane
    R, _ = cv2.Rodrigues(rvec)
    # plane normal vector
    plane_normal = R[2, :]
    # a point on the plane
    plane_point = np.reshape(tvec, (3,))
    # for each point in normalized image plane
    for normalized_point in normalized_points:
        ray_point = np.reshape(normalized_point, (3,))
        # direction of the ray
        ray_dir = ray_point / np.linalg.norm(ray_point)
        # dot product of plane normal and ray direction
        raydir_dot_planenormal = plane_normal.dot(ray_dir)
        # if plane normal and ray direction are orthogonal the plane is
        # parallel to the ray -> no intersection exists
        if abs(raydir_dot_planenormal) < 1e-06:
            print("plane is almost parallel to ray")
            return None
        # calculate from normalized image plane to the intersection
        ray_to_plane_dist = ((plane_point - ray_point).dot(plane_normal) /
                             raydir_dot_planenormal)
        object_point = ray_point + ray_to_plane_dist * ray_dir
        object_point = np.reshape(object_point, (1, 3))
        object_points = np.append(object_points, object_point, axis=0)
    return object_points


def get_charuco_corners(image, board):
    """
    Detects the charuco board in the image and estimates its pose.

    Parameters
    ----------
    image : InputArrayOfArrays
        possibly contains a charuco board
    board : CharucoBoard
        the board to detect

    Returns
    -------
    ret : bool
        True on success, False on fail
    charuco_corners : OutputArray
        interpolated chessboard corners
    charuco_ids : OutputArray
        identifiers of the interpolated chessboard corners
    """
    # find aruco markers
    marker_corners, marker_ids, _ = ar.detectMarkers(
        image, board.dictionary)
    if marker_corners is None or len(marker_corners) == 0:
        return False, None, None
    # use the markers to interpolate the corners of the board
    return ar.interpolateCornersCharuco(marker_corners, marker_ids,
                                        image, board)


def get_charuco_pose(image, board, K, d):
    """
    Detects the charuco board in the image and estimates its pose.

    Parameters
    ----------
    image : InputArrayOfArrays
        possibly contains a charuco board
    board : CharucoBoard
        the board to detect
    K : InputArrayOfArrays
        the camera intrinsic matrix
    d : InputArray
        the distortion coefficients of the camera

    Returns
    -------
    ret : bool
        True on success, False on fail
    rvec : OutputArray
        Rodrigues rotation vector of the charuco board pose
        None on fail
    tvec : OutputArray
        translation vector of the charuco board pose
        None on fail
    """
    # find aruco markers
    marker_corners, marker_ids, _ = ar.detectMarkers(
        image, board.dictionary)
    if marker_corners is None or len(marker_corners) == 0:
        return False, None, None
    # use the markers to interpolate the corners of the board
    retval, charuco_corners, charuco_ids = get_charuco_corners(image, board)
    # check if interpolation succeeded
    if not retval:
        return False, None, None
    # enough corners for a diamond?
    if len(charuco_corners) < 4:
        return False, None, None
    # calculate the pose
    return ar.estimatePoseCharucoBoard(charuco_corners, charuco_ids, board,
                                       K, d)


class CharucoDetector:
    """
    Detects image and object points using a color camera and a charuco board.
    """

    def __init__(self, camera, K_Cam, d_cam, calibration_pattern, board):
        """
        Parameters
        ----------
        camera : Camera
            provides a read method
        K_cam : InputArrayOfArrays
            the intrinsic matrix of the camere
        d_cam : InputArray
            the distortion coefficients of the camera
        calibration_pattern : CalibrationPattern
            displayed on projector, provides detect_image_points,
            draw_image_points and get_object_points methods
        board : CharucoBoard
            used to detect the plane of the image points
        """
        self.camera = camera
        self.K_cam = K_Cam
        self.d_cam = d_cam
        self.pattern = calibration_pattern
        self.board = board

    def next_frame(self):
        """
        Detects the object points of the circles pattern using the realsense
        aligned depth image.

        Returns
        -------
        frame : Image
            the annotated color image
        cam_image_points : OutputArrayOfArrays
            the image points of the color camera or None on fail
        proj_image_points : OutputArrayOfArrays
            the image_points of the projector or None on fail
        object_points : OutputArrayOfArrays
            the object points in the camera frame or None on fail
        """
        # get the next frames from the realsense
        retval, color_frame = self.camera.read()
        if not retval:
            return color_frame, None, None, None
        # detect the board
        found_board, rvec, tvec = get_charuco_pose(color_frame, self.board,
                                                   self.K_cam, self.d_cam)
        if not found_board:
            return color_frame, None, None, None
        # visualize pose
        color_frame = cv2.drawFrameAxes(
            color_frame, self.K_cam, self.d_cam, rvec, tvec, 0.034)
        # detect the asymmetric circles pattern
        found_pattern, image_points = self.pattern.detect_image_points(
            color_frame)
        if not found_pattern:
            return color_frame, None, None, None
        # calculate the object points
        object_points = calc_object_points(image_points, rvec, tvec,
                                           self.K_cam, self.d_cam)
        # visualize
        color_frame = self.circles_grid.draw_centers(color_frame)
        return (color_frame, image_points, self.pattern.get_image_points(),
                object_points)

    def skip_frame(self):
        """
        Reads the next frame but does not process it.


        Returns
        -------
        frame : Image
            the annotated color image of the detection
        """
        _, color_frame = self.camera.read()
        return color_frame
