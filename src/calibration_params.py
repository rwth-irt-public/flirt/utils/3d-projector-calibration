""" Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. """

from cv2 import FileStorage, FileStorage_READ, FileStorage_WRITE
import numpy as np


class CalibrationParams:

    def __init__(self):
        self.name = "projector"
        self.image_size = (1920, 1080)
        # number of views on the calibration pattern
        self.n_views = 20
        # camera intrinsics
        self.K_cam = np.identity(3, dtype=np.float32)
        self.d_cam = np.array([0, 0, 0, 0, 0], dtype=np.float32)
        # time to wait between each frame
        self.wait_time = 1
        # skip this number of frames (n % (skip_frames + 1) == 0 is processed)
        self.skip_frames = 3

    def load(filename):
        """
        Create the CalibrationParams from a file

        Returns
        -------
        The loaded CalibrationParams
        """
        file = FileStorage(filename, FileStorage_READ,
                           encoding="utf-8")
        params = CalibrationParams()
        params.name = file.getNode("name").string()
        image_width = int(file.getNode("image_width").real())
        image_height = int(file.getNode("image_height").real())
        params.image_size = (image_width, image_height)
        params.n_views = int(file.getNode("n_views").real())
        params.K_cam = file.getNode("K_cam").mat()
        params.d_cam = file.getNode("d_cam").mat()
        params.wait_time = int(file.getNode("wait_time").real())
        params.skip_frames = int(file.getNode("skip_frames").real())
        file.release()
        return params

    def save(self, filename):
        file = FileStorage(filename, FileStorage_WRITE,
                           encoding="utf-8")
        file.write("name", self.name)
        file.write("image_width", self.image_size[0])
        file.write("image_height", self.image_size[1])
        file.write("n_views", self.n_views)
        file.write("K_cam", self.K_cam)
        file.write("d_cam", self.d_cam)
        file.write("wait_time", self.wait_time)
        file.write("skip_frames", self.skip_frames)
        file.release()


def main():
    calib_params = CalibrationParams()
    calib_params.save("data/params_test.yml")
    calib_params = CalibrationParams.load(
        "data/projector_1920x1080_params.yml")
    print(calib_params)


# run the main prgram
if __name__ == "__main__":
    main()
