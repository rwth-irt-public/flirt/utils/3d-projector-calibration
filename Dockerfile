FROM python:3-slim as base

# Set the working directory to /app
WORKDIR /app

# install system dependencies
RUN apt-get update && apt-get install -y \
    libgtk2.0-dev

# install python dependencies
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

# Copy the source code
COPY ./src /app

# Run the calibration when the container launches
CMD ["python", "-u", "calibrate_camera.py"]