# Table of Contents
[[_TOC_]]

# Requirements
Electronics:
- 3D-camera which does **not** use any visible patterns (e.g. structured light).
  We use a realsense camera and the code heavily depends on the Realsense2 API.
  Abstracting away the camera might be a nice contribution ;)
- tabletop projector

Mechanical one of either:
- A mechanical mount which fixes the relative position of the camera and the projector + a flat surface (like a wall)
- Projector and camera and a tripod + a large sheet of cardboard

# Setup
Create a virtual environment for Python3 and install the *requirements.txt*:
```bash
python3 -m venv calib_venv
source calib_venv/bin/activate
pip install -r requirements.txt 
```

# How it works
Basically, the projector can be seen as an inverse camera.
Therefore, we can use the [OpenCV stereo calibration](https://docs.opencv.org/master/d9/d0c/group__calib3d.html#ga91018d80e2a93ade37539f01e6f07de5) to calibrate the intrinsic and extrinsic parameters of both cameras.
Ideally the first 3D-camera is already calibrated which reduces the complexity of the optimization.

Instead of using a physical calibration pattern of known size to establish 2D to 3D correspondences, we use the projector to project a circles grid with known 2D pixel coordinates.
A 3D camera (realsense) is used to find the corresponding 3D object points of the projected pattern.
Multiple different viewing angles have to be recorded for the optimization.
Finally OpenCVs `stereoCalibrate` estimates the projectors intrinsic and extrinsic parameters based on the 2D and 3D points.

The main entry points are the files:
- *calibrate_rs_device.py* - record a sequence of images from a connected device
- *calibrate_rs_file.py* - load a ROSbag file which contains the images

# Special Thanks
- The simple workflow for calibrating a projector via OpenCV: 
http://www.morethantechnical.com/2017/11/17/projector-camera-calibration-the-easy-way/
- ARSandbox for the idea of using a 3D camera to capture object points:
https://arsandbox.ucdavis.edu/
- Later found this article which does basically the same:
https://bingyaohuang.github.io/Calibrate-Kinect-and-projector/

# Citing
If you use this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.
